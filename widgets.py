# -*- coding: utf-8 -*-
"""
    @package erp.modules.jsonwidget.widgets
    ~~~~~~~~~~~~~

    A description which can be long and explain the complete
    functionality of this module even with indented code examples.
    Class/Function however should not be documented here.

    @author Ewgeniy Hramkow (ewgenius)
    @version 0.0.1
    @date 30-01-2014

    :copyright: year by Ewgeniy Hramkow (ewgenius), see AUTHORS for more details
    :license: YLP, see LICENSE for more details
"""
from django.forms.widgets import Widget
import simplejson
from django import utils
import copy


class QuizAdminWidget(Widget):

    def _to_json_string(self, name, data):
        result = []
        copy_data = copy.deepcopy(data)

        def check_page(pageid):
            for page in result:
                if page['id'] == pageid:
                    return True
            return False

        nodes = dict(copy_data.iteritems())
        keys = nodes.keys()

        def sortItems(input):
            items = input.split('_')
            if items[0] == 'page':
                return '0' + items[1]
            elif items[0] == 'question':
                return '1' + items[2]
            elif items[0] == 'option':
                return '2' + items[3]
            elif items[0] == 'way':
                return '3' + items[2]
            elif items[0] == 'wayname':
                return '4' + items[2]
            else:
                return '5'

        keys.sort(key=sortItems)

        for key in keys:
            items = key.split('_')
            if items[0] == 'page':
                pageid = items[1]
                page = {
                    'id': pageid,
                    'template': [],
                    'ways': []
                }
                result.append(page)
            elif items[0] == 'question':
                pageid = items[1]
                questionid = items[2]
                type = items[3]

                question = {
                    'body': nodes[key],
                    'id': questionid,
                    'type': int(type)
                }

                if type == 0:
                    question['answer'] = ""
                else:
                    question['answer'] = []

                result[int(pageid)]['template'].append(question)
            elif items[0] == 'option':
                pageid = items[1]
                questionid = items[2]

                result[int(pageid)]['template'][int(questionid)]['answer'].append({
                    'option': nodes[key],
                    'value': False
                })
            elif items[0] == 'way':
                pageid = items[1]
                wayid = items[2]
                result[int(pageid)]['ways'].append({
                    'id': nodes[key],
                    'value': ""
                })
            elif items[0] == 'wayname':
                pageid = items[1]
                wayid = items[2]
                result[int(pageid)]['ways'][int(wayid)]['value'] = nodes[key]
        return result

    def value_from_datadict(self, data, files, name):
        data_copy = copy.deepcopy(data)
        result = self._to_json_string(name, data_copy)
        return simplejson.dumps(result)

    def render(self, name, value, attrs=None):
        result = '<div class="quizwidget" id="id_questions">'

        button_delete = u'<a href="javascript:;" class="delete">&times;</a>'
        button_collapse = u'<a href="javascript:;" class="collapse">_</a>'

        try:
            for page in value:
                result += '<div class="quizpage"><div class="pageheader">'
                result += '<input type="hidden" name="page_%s"/>' % page['id']
                result += u'<div class="pageid">%s</div>' % page[
                    'id'] + button_delete + button_collapse
                result += '</div><div class="questions">'
                template = page['template']
                result += u'<div class="pagesection"><div>Вопросы</div></div>'
                ind = 0

                if len(template) > 0:
                    for question in template:
                        result += '<div class="question">'
                        result += button_delete
                        result += '<input id="question_%s_%s_%s" name="question_%s_%s_%s" value="%s">' % (
                            page['id'], question['id'], question['type'], page['id'], question['id'], question['type'], question['body'])
                        result += '<br><div class="answer">'
                        if not int(question['type']) == 0:
                            ind1 = 0
                            for option in question['answer']:
                                result += '<div class="option">'
                                result += (u'<input id="option_%s_%s_%s" name="option_%s_%s_%s" value="%s"/>' + button_delete + '<br>') % (
                                    page['id'], question['id'], str(ind1), page['id'], question['id'], str(ind1), option['option'])
                                ind1 += 1
                                result += u'</div>'
                            result += u'<br><a href="javascript:;" class="button">добавить опцию</a><br>'
                        result += u'</div>'

                        result += '</div>'
                        ind += 1
                result += u'<br><a href="javascript:;" class="button">добавить вопрос</a>'
                result += u'<select id="question_type">\
                    <option>Текст</option>\
                    <option>Несколько вариантов</option>\
                    <option>Выбор одного варианта</option>\
                    </select></div>'

                result += '<div class="ways">'
                result += u'<div class="pagesection"><div>Варианты</div></div>'
                ind = 0

                ways = page['ways']
                if len(ways) > 0:
                    for way in page['ways']:
                        result += '<div class="way">'
                        result += button_delete

                        result += '<input id="wayname_%s_%s" name="wayname_%s_%s" value="%s">' % (
                            page['id'], ind, page['id'], ind, way['value'])
                        result += '<input id="way_%s_%s" name="way_%s_%s" value="%s">' % (
                            page['id'], ind, page['id'], ind, way['id'])

                        result += '</div>'
                        ind += 1
                result += u'<br><a href="javascript:;" class="button">добавить вариант</a></div>'

                result += u'</div>'
        except Exception as e:
            print '\n Error %s' % e

        result += u'<a href="javascript:;" class="button addpage">добавить страницу</a></div>'
        return utils.safestring.mark_safe(result)


class AnswersAdminWidget(Widget):

    def __init__(self, attrs=None):
        super(AnswersAdminWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        result = '<div class="answerswidget" id="id_answers">'
        try:
            #from erp.projects.models import Quiz
            templates = value['quiz']
            for page in value['answers']:
                result += '<div class="quizpage">'
                for question in page['questions']:
                    template = templates[int(question['pageid'])][
                        'template'][int(question['questionid'])]
                    result += '<div class="question">'
                    result += u'<div class="questionbody">%s</div>' % template[
                        'body']
                    result += u'<div class="questionanswer">'
                    if template['type'] == 0:
                        result += question['answer']
                    elif template['type'] == 1:
                        result += '<ul>'
                        for variant in question['answer']:
                            result += '<li>%s</li>' % template[
                                'answer'][int(variant)]['option']
                        result += '</ul>'
                    elif template['type'] == 2:
                        result += template['answer'][
                            int(question['answer'][0])]['option']
                    result += '</div>'
                    result += '</div>'
                result += '</div>'
                if 'way' in page.keys():
                    step = templates[int(question['pageid'])][
                        'ways'][int(page['way'])]['value']
                    result += '<div class="step"><br>%s</div>' % step
        except Exception as e:
            print '\n Error %s' % e
        result += '</div>'
        return utils.safestring.mark_safe(result)


class JsonListWidget(Widget):

    def __init__(self, attrs=None):
        if attrs is not None:
            self.attrs = attrs.copy()
        else:
            self.attrs = {}

    def _to_json_string(self, name, data):
        result = []
        copy_data = copy.deepcopy(data)

        nodes = dict(copy_data.iteritems())
        keys = nodes.keys()

        for key in keys:
            items = key.split('__')
            if items[0] == 'listitem' and items[1] == name:
                result.append({
                    'value': nodes[key]
                })

        return result

    def value_from_datadict(self, data, files, name):
        data_copy = copy.deepcopy(data)
        result = self._to_json_string(name, data_copy)
        return simplejson.dumps(result)

    def render(self, name, value, attrs=None):
        final_attrs = self.build_attrs(attrs, name=name)
        result = '<div class="listjsonwidget"><ul id="list__%s">' % final_attrs['name']

        button_delete = u'<a href="javascript:;" class="delete">&times;</a>'

        # try:
        ind = 0
        if type(value) is not list:
            value = simplejson.loads(value)
        for item in value:
            result += u'<li><input name="listitem__%s__%s" value="%s">%s</li>' % (final_attrs['name'], ind, item['value'], button_delete)
            ind += 1

        # except Exception as e:
        #    print '\n Error in %s %s' % (self, e)

        result += u'</ul><br><a href="javascript:;" class="add">добавить</a></div>'

        return utils.safestring.mark_safe(result)


class JsonDetailsListWidget(Widget):

    def __init__(self, attrs=None):
        if attrs is not None:
            self.attrs = attrs.copy()
        else:
            self.attrs = {}

    def _to_json_string(self, name, data):
        result = {
            'headers': [],
            'values': []
        }
        copy_data = copy.deepcopy(data)

        nodes = dict(copy_data.iteritems())
        keys = nodes.keys()

        for key in keys:
            items = key.split('__')
            if items[0] == 'header' and items[1] == name:
                result['headers'].append({
                    'name': nodes[key]
                })
            elif items[0] == 'data' and items[1] == name:
                index = int(items[2])
                if len(result['values']) <= index:
                    for i in range(1 + index - len(result['values'])):
                        result['values'].append({})

                result['values'][index][items[3]] = nodes[key]

        return result

    def value_from_datadict(self, data, files, name):
        data_copy = copy.deepcopy(data)
        result = self._to_json_string(name, data_copy)
        return simplejson.dumps(result)

    def render(self, name, value, attrs=None):
        final_attrs = self.build_attrs(attrs, name=name)
        button_delete_row = u'<a href="javascript:;" class="delete_row">&times;</a>'
        button_delete_column = u'<a href="javascript:;" class="delete_column">&times;</a>'
        button_add_row = u'<a href="javascript:;" class="add_detail">+</a>'
        button_add_column = u'<a href="javascript:;" class="add_column">+</a>'
        result = '<div class="jsondetailslistwidget"><table id="list__%(name)s"><tr>' % {'name': final_attrs['name']}

        try:
            if type(value) is not list and type(value) is not dict:
                value = simplejson.loads(value)

            headers = value['headers']

            for item in headers:
                result += '<th><input name="header__%(widget)s__%(name)s" value="%(name)s"> %(delete)s</th>' % {
                    'widget': final_attrs['name'],
                    'name': item['name'],
                    'delete': button_delete_column,
                }
            result += u'<th class="th_add">%s</th></tr>' % button_add_column
            ind = 0
            for row in value['values']:
                result += '<tr class="row">'
                for header in headers:
                    result += '<td><input name="data__%(widget)s__%(index)s__%(name)s" value="%(value)s"></td>' % {
                        'widget': final_attrs['name'],
                        'name': header['name'],
                        'value': row[header['name']],
                        'index': ind
                    }
                ind += 1
                result += u'<td>%s</td></tr>' % (button_delete_row)

            ind = 0
        except Exception as e:
            print 'render: %s' % e

        # for item in value['values']:
        #    result += u'<tr><td><input name="listitem__%s__%s" value="%s"></td>' % (final_attrs['name'], ind, '')
        #    for detail in item['details']:
        #        result += u'<td><input name="listitem__%s_detail" value=""></td>' % ind
        #    result += u'<td>%s</td><td>%s</td></tr>' % (button_add, button_delete_row)
        #    ind += 1

        result += u'</table><br><a href="javascript:;" class="button add">добавить</a></div>'
        return utils.safestring.mark_safe(result)
