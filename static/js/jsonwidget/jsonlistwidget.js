(function() {
    jQ = null;
    if(window.django !== undefined)
        jQ = django.jQuery
    else
        jQ = $

    function deleteItem() {
        jQ(this).parent().remove();
    }

    function addItem() {
        var list = jQ(this).parent().children('ul')
        var id = list.attr('id').replace('list__', '');
        var length = list.children().length;
        list.append(
            jQ('<li>')
            .append(jQ('<input>')
                .attr('name','listitem__' + id + '__' + length))
            .append(jQ('<a>')
                .addClass('delete')
                .attr('href', 'javascript:;')
                .click(deleteItem)
                .html('x')));
    }

    jQ(window).ready(function(){
        widget = jQ('listjsonwidget');

        jQ('.listjsonwidget').find('.add').click(addItem);
        jQ('.listjsonwidget').find('.delete').click(deleteItem);
    });
})();
/*


<div class="quizpage">Страница<br>
<label id="page_3">id 3</label>
<a href="#" class="button delete">x</a><div class="questions"><br>Вопросы<br><br><a href="#" class="button">добавить вопрос</a></div><div class="ways"><br>Варианты<br><br><a href="#" class="button">добавить вариант</a></div></div>*/
