(function() {
    jQ = null;
    if(window.django !== undefined)
        jQ = django.jQuery;
    else
        jQ = $;

    var widget_name = '';

    function deleteRow() {
        var container = jQ(this).parent().parent().parent();
        jQ(this).parent().parent().remove();
        updateIndices(container.find('.row'));
    }

    function deleteColumn() {
        var rows = jQ(this).parent().parent().siblings();
        var i = jQ(this).parent().index();
        rows.each(function(index, row) {
                jQ(row.children[i]).remove();
            });
        jQ(this).parent().remove();
    }

    function addRow() {
        var table = jQ(this).parent().find('table');
        var row = jQ('<tr>').addClass('row');
        jQ(table.find('tr')[0]).find('th>input').each(function(index, input) {
            var name = jQ(input).attr('name').split('__');
            row.append(jQ('<td>')
                .append(jQ('<input>')
                    .attr('name', ['data', name[1], 0, name[2]].join('__'))));
        });
        row.append(jQ('<td>')
            .append(jQ('<a href="javascript:;" class="delete_row">&times;</a>')
                .click(deleteRow)));
        table.append(row);
        updateIndices(table.find('.row'));
    }

    function addColumn() {
        var rows = jQ(this).parent().parent().siblings();
        var i = jQ(this).parent().index();
        //var name = rows.parent().parent().attr('id').split('__')[1];
        rows.each(function(index, row) {
            jQ(row.children[i]).before(jQ('<td>')
                .append(jQ('<input>')
                    .attr('name', 'data__' + widget_name + '__' + index + '__column_' + i)));
        });
        jQ(this).parent().before(jQ('<th>')
            .append(jQ('<input>')
                .attr('name', 'header__' + widget_name + '__column_' + i)
                .val('column_' + i)
                .keyup(editHeader))
            .append(jQ('<a href="javascript:;" class="delete_column">&times;</a>')
                .click(deleteColumn)));
    }

    function updateIndices(rows) {
        //var name = rows.parent().parent().attr('id').split('__')[1];

        rows.each(function(index, row) {
            jQ(row).find('td>input').each(function(index1, input) {
                var name = jQ(input).attr('name').split('__');
                name[2] = index;
                jQ(input).attr('name', name.join('__'));
            });
        });
    }

    function editHeader() {
        var header = jQ(this).attr('name').split('__');
        header[2] = $(this).val();
        jQ(this).attr('name', header.join('__'));

        var rows = jQ(this).parent().parent().siblings();
        var i = jQ(this).parent().index();
        var self = this;
        rows.each(function(index, row) {
            var input = jQ(row.children[i]).find('input');
            var name = input.attr('name').split('__');
            name[3] = jQ(self).val();
            input.attr('name', name.join('__'));
        });
    }

    function uploadFile() {
        var popup = jQ('<div id="popup"><form method="post"><input type="file"><br><input type="submit" value="Импортировать"><form></div>');
        console.info(popup);
        jQ('body').append(popup);
    }

    jQ(window).ready(function(){
        widget_name = jQ('.jsondetailslistwidget').find('table').attr('id').split('__')[1];
        jQ('.jsondetailslistwidget').find('.add').click(addRow);
        jQ('.jsondetailslistwidget').find('.add_column').click(addColumn);
        jQ('.jsondetailslistwidget').find('.delete_row').click(deleteRow);
        jQ('.jsondetailslistwidget').find('.delete_column').click(deleteColumn);
        jQ('.jsondetailslistwidget').find('th>input').keyup(editHeader);
        $('.jsondetailslistwidget').find('.import').magnificPopup({
            type: 'ajax',
            closeOnContentClick: false,
            closeOnBgClick: false
        });
    });
})();
/*
<div class="quizpage">Страница<br>
<label id="page_3">id 3</label>
<a href="#" class="button delete">x</a><div class="questions"><br>Вопросы<br><br><a href="#" class="button">добавить вопрос</a></div><div class="ways"><br>Варианты<br><br><a href="#" class="button">добавить вариант</a></div></div>*/
