jQ = null;
if(window.django !== undefined)
    jQ = django.jQuery
else
    jQ = $

var buttonDelete = jQ('<a>')
.addClass('delete')
.attr('href', 'javascript:;')
.html('&times;');

var buttonCollapse = jQ('<a>')
.addClass('collapse')
.attr('href', 'javascript:;')
.html('_');

var button = jQ('<a>')
.addClass('button')
.attr('href', 'javascript:;');

function collapseContainer() {
    var parent = jQ(this).parent().parent();
    if(parent.hasClass('collapsed')) {
        parent.removeClass('collapsed');
        jQ(this).html('_');
    }
    else {
        parent.addClass('collapsed');
        jQ(this).html('+');
    }
}

function deleteItem() {
    var parent = jQ(this).parent();
    var grand = parent.parent();
    if(parent.hasClass('pageheader')) {
        parent = jQ(parent).parent();
        parent.remove();
        var pages = jQ('.quizpage');
        for (var i = 0; i < pages.length; i++) {
            jQ(pages[i]).children('.pageheader').children('.pageid').html(i);
            jQ(pages[i]).children('.pageheader').children('input').attr('name', 'page_' + i);
        };
    } else {
        parent.remove();
    }

    if(parent.hasClass('option')) {
        updateOptions(grand);
    } else if(parent.hasClass('question')) {
        udpateQuestions(grand);
    } else if(parent.hasClass('way')) {
        updateWays(grand);
    }
}

function updateOptions(element) {
    var answer = element;
    var question = answer.parent();
    var questions = question.parent();
    var page = questions.parent();
    var pageId = page.children('.pageheader').children('.pageid').html();
    var questionId = questions.children('.question').index(question);//question.index(questions.children('.question'));

    var options = answer.children('.option');
    for(var i = 0; i < options.length; i++) {
        jQ(options[i]).children('input')
        .attr('id', 'option_' + pageId + '_' + questionId + '_' + i)
        .attr('name', 'option_' + pageId + '_' + questionId + '_' + i);
    }
}

function addOption() {
    jQ(this).before(jQ('<div>')
        .addClass('option')
        .append(jQ('<input>'))
        .append(buttonDelete.clone().click(deleteItem)));
    updateOptions(jQ(this).parent());
}

function udpateQuestions(element) {
    var questions = element;
    var page = questions.parent();
    var pageId = page.children('.pageheader').children('.pageid').html();

    var q = questions.children('.question');
    for(var i = 0; i < q.length; i++) {
        jQ(q[i]).children('input')
        .attr('id', 'question_' + pageId + '_' + i + '_' + type)
        .attr('name', 'question_' + pageId + '_' + i + '_' + type);
        updateOptions(jQ(q[i]).children('.answer'));
    }
}

function addQuestion() {
    var questions = jQ(this).parent();
    var page = questions.parent();
    var pageId = page.children('.pageheader').children('.pageid').html();
    var questionId = questions.children('.question').length;

    var type = questions.children('select').children(':selected').index();
    var question = jQ('<div>')
    .addClass('question')
    .append(buttonDelete.clone().click(deleteItem))
    .append(jQ('<input>')
        .attr('id', 'question_' + pageId + '_' + questionId + '_' + type)
        .attr('name', 'question_' + pageId + '_' + questionId + '_' + type));
    console.info('question_' + pageId + '_' + questionId + '_' + type);
    var answer = jQ('<div>').addClass('answer');


    if(type != 0) {
        answer.append(jQ('<div>')
            .addClass('option')
            .append(jQ('<input>')
                .attr('id', 'option_' + pageId + '_' + questionId + '_0')
                .attr('name', 'option_' + pageId + '_' + questionId + '_0'))
            .append(buttonDelete.clone().click(deleteItem)))
        .append(button.clone()
            .click(addOption)
            .html('добавить опцию'));
    }
    question.append(answer);
    //question.draggale({snap: true});
    jQ(this).before(question);

    udpateQuestions(questions);
}

function updateWays(element) {
    var ways = element;
    var question = ways.parent();
    var questionId = question.children('.pageheader').children('.pageid').html();
    var w = ways.children('.way');
    for (var i = 0; i < w.length; i++) {
        jQ(jQ(w[i]).children('input')[0])
        .attr('id', 'wayname_' + questionId + '_' + i)
        .attr('name', 'wayname_' + questionId + '_' + i);
        jQ(jQ(w[i]).children('input')[1])
        .attr('id', 'way_' + questionId + '_' + i)
        .attr('name', 'way_' + questionId + '_' + i);
    }
}

function addWay() {
    var ways = jQ(this).parent();
    var question = ways.parent();
    var questionId = question.children('.pageheader').children('.pageid').html();
    var wayId = ways.children('.way').length;
    var way = jQ('<div>')
    .addClass('way')
    .append(buttonDelete.clone().click(deleteItem))
    .append(jQ('<input>')
        .attr('id', 'wayname_' + questionId + '_' + wayId)
        .attr('name', 'wayname_' + questionId + '_' + wayId))
    .append(jQ('<input>')
        .attr('id', 'way_' + questionId + '_' + wayId)
        .attr('name', 'way_' + questionId + '_' + wayId));
    jQ(this).before(way);
}

function addPage() {
    var page = jQ('<div>')
    .addClass('quizpage')
    .append(jQ('<div>')
        .addClass('pageheader')
        .append(jQ('<input>').
            attr('type', 'hidden').
            attr('name', 'page_' + jQ('.quizpage').size()))
        .append(jQ('<div>')
            .addClass('pageid')
            .html(jQ('.quizpage').size()))
        .append(buttonDelete.clone().click(deleteItem))
        .append(buttonCollapse.clone().click(collapseContainer)))
    .append(jQ('<div>')
        .addClass('questions')
        .append(jQ('<div>')
            .addClass('pagesection')
            .append(jQ('<div>').html('Вопросы')))
        .append(button.clone()
            .click(addQuestion)
            .html('добавить вопрос'))
        .append(jQ('<select>')
            .append(jQ('<option>').html('Текст'))
            .append(jQ('<option>').html('Несколько вариантов'))
            .append(jQ('<option>').html('Выбор одного варианта'))))
    .append(jQ('<div>')
        .addClass('ways')
        .append(jQ('<div>')
            .addClass('pagesection')
            .append(jQ('<div>').html('Варианты')))
        .append(button.clone()
            .click(addWay)
            .html('добавить вариант')))
    jQ(this).before(page);
}

jQ(window).ready(function(){
    widget = jQ('#id_questions');

    jQ('.delete').click(deleteItem);
    jQ('.collapse').click(collapseContainer);
    jQ('.questions>.button').click(addQuestion);
    jQ('.ways>.button').click(addWay);
    jQ('.answer>.button').click(addOption);

    widget.children('.button').click(addPage);
});
/*
<div class="quizpage">Страница<br>
<label id="page_3">id 3</label>
<a href="#" class="button delete">x</a><div class="questions"><br>Вопросы<br><br><a href="#" class="button">добавить вопрос</a></div><div class="ways"><br>Варианты<br><br><a href="#" class="button">добавить вариант</a></div></div>*/
